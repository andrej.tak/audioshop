package net.stawrul.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

/**
 * Klasa encyjna reprezentująca towar w sklepie (płytę CD).
 */
@Entity
@EqualsAndHashCode(of = "id")
@NamedQueries(value = { @NamedQuery(name = AudioCD.FIND_ALL, query = "SELECT b FROM AudioCD b") })
public class AudioCD
{
    public static final String FIND_ALL = "AudioCD.FIND_ALL";

    @Getter
    @Setter
    @Id
    UUID id = UUID.randomUUID();

    @Getter
    @Setter
    String author;
    
    @Getter
    @Setter
    String title;

    @Getter
    @Setter
    Integer tracks;
    
    @Getter
    @Setter
    String genre;
    
    @Getter
    @Setter
    Integer amount;
    
    @Getter
    @Setter
    @Column(nullable = false, precision = 7, scale = 2, columnDefinition = "DECIMAL(7, 2)")
    Double cost;
}
