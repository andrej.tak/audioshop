package net.stawrul.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.UUID;
import net.stawrul.model.AudioCD;
import net.stawrul.services.AudioCDService;
import org.springframework.http.HttpStatus;

import static org.springframework.http.HttpStatus.CONFLICT;

@RestController
@RequestMapping("/audiocds")
public class AudioCDsController
{
    //Komponent realizujący logikę biznesową operacji na książkach
    final AudioCDService audiocdsService;

    //Instancja klasy BooksService zostanie dostarczona przez framework Spring
    //(wstrzykiwanie zależności przez konstruktor).
    public AudioCDsController(AudioCDService cdService)
    {
        this.audiocdsService = cdService;
    }

    @GetMapping
    public List<AudioCD> listAudioCDs() {
        return audiocdsService.findAll();
    }

    @PostMapping
    public ResponseEntity<Void> addAudioCD(@RequestBody AudioCD cd, UriComponentsBuilder uriBuilder)
    {
        if (audiocdsService.find(cd.getId()) == null)
        {
            try
            {
                if(cd.getCost() < 0.0)
                {
                    return ResponseEntity.status(HttpStatus.PAYMENT_REQUIRED).build();
                }
            }
            catch(NullPointerException e)
            {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
            }
            
            audiocdsService.save(cd);

            URI location = uriBuilder.path("/books/{id}").buildAndExpand(cd.getId()).toUri();
            return ResponseEntity.created(location).build();
        }
        else
        {
            //Identyfikator książki już istnieje w bazie danych. Żądanie POST służy do dodawania nowych elementów,
            //więc zwracana jest odpowiedź z kodem błędu 409 Conflict
            return ResponseEntity.status(CONFLICT).build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<AudioCD> getAudioCD(@PathVariable UUID id)
    {
        AudioCD cd = audiocdsService.find(id);

        //W warstwie biznesowej brak książki o podanym id jest sygnalizowany wartością null. Jeśli książka nie została
        //znaleziona zwracana jest odpowiedź 404 Not Found. W przeciwnym razie klient otrzymuje odpowiedź 200 OK
        //zawierającą dane książki w domyślnym formacie JSON
        return cd != null ? ResponseEntity.ok(cd) : ResponseEntity.notFound().build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> updateCd(@RequestBody AudioCD cd)
    {
        if (audiocdsService.find(cd.getId()) != null)
        {
            audiocdsService.save(cd);
            return ResponseEntity.ok().build();

        }
        return ResponseEntity.notFound().build();
    }
}
