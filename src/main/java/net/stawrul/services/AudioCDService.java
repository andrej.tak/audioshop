package net.stawrul.services;

import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;
import net.stawrul.model.AudioCD;

/**
 * Komponent (serwis) biznesowy do realizacji operacji na płytach.
 */
@Service
public class AudioCDService extends EntityService<AudioCD>
{
    //Instancja klasy EntityManger zostanie dostarczona przez framework Spring
    //(wstrzykiwanie zależności przez konstruktor).
    public AudioCDService(EntityManager em)
    {
        super(em, AudioCD.class, AudioCD::getId);
    }

    /**
     * Pobranie wszystkich płyt z bazy danych.
     *
     * @return lista płyt
     */
    public List<AudioCD> findAll()
    {
        return em.createNamedQuery(AudioCD.FIND_ALL, AudioCD.class).getResultList();
    }
}
