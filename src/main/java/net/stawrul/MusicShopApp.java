package net.stawrul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Klasa definiująca punkt wejścia aplikacji.
 */
@SpringBootApplication
public class MusicShopApp
{
    public static void main(String[] args)
    {
        SpringApplication.run(MusicShopApp.class, args);
    }
}
