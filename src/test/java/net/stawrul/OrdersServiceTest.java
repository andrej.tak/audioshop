package net.stawrul;

import net.stawrul.model.Order;
import net.stawrul.services.OrdersService;
import net.stawrul.services.exceptions.OutOfStockException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.EntityManager;
import net.stawrul.model.AudioCD;
import net.stawrul.services.exceptions.DoesNotExistException;
import net.stawrul.services.exceptions.NotEnoughProductsInOrder;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class OrdersServiceTest
{
    @Mock
    EntityManager em;

    // Test 1 - OutOfStockException
    @Test(expected = OutOfStockException.class)
    public void whenOrderedCdNotAvailable_placeOrderThrowsOutOfStockEx()
    {
        // Arrange
        Order order = new Order();
        AudioCD cdInStore = new AudioCD();
        cdInStore.setAmount(0);

        AudioCD cd = new AudioCD();
        cd.setId(cdInStore.getId());
        cd.setAmount(1);

        order.getAudioCds().add(cd);

        Mockito.when(em.find(AudioCD.class, cd.getId())).thenReturn(cdInStore);

        OrdersService ordersService = new OrdersService(em);

        // Act
        ordersService.placeOrder(order);

        // Assert - exception expected
    }
    
    // Test 2 - Buy one cd, get one cd
    
    @Test
    public void whenOrderedCdAvailable_placeOrderDecreasesAmountByOne()
    {
        // Arrange
        Order order = new Order();
        AudioCD cdInStore = new AudioCD();
        cdInStore.setAmount(1);

        AudioCD cd = new AudioCD();
        cd.setId(cdInStore.getId());
        cd.setAmount(1);

        order.getAudioCds().add(cd);

        Mockito.when(em.find(AudioCD.class, cd.getId())).thenReturn(cdInStore);

        OrdersService ordersService = new OrdersService(em);

        // Act
        ordersService.placeOrder(order);

        // Assert
        //dostępna liczba książek zmniejszyła się:
        assertEquals(0, (int)cdInStore.getAmount());
        //nastąpiło dokładnie jedno wywołanie em.persist(order) w celu zapisania zamówienia:
        Mockito.verify(em, times(1)).persist(order);
    }
}

