package net.stawrul;

import java.net.URI;
import java.net.URISyntaxException;
import javax.persistence.EntityManager;
import net.stawrul.controllers.AudioCDsController;
import net.stawrul.model.AudioCD;
import net.stawrul.services.AudioCDService;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.Matchers.any;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import org.springframework.http.HttpStatus;
import static org.springframework.http.HttpStatus.CONFLICT;

@RunWith(MockitoJUnitRunner.class)
public class AudioCDServiceTest
{
    @Mock
    EntityManager em;
    
    // Test 1 - crash when there is no cost defined
    @Test
    public void whenAddCdWithNoCost_statusReturnsBadRequest() throws URISyntaxException
    {
        // Arrange
        AudioCD cd = new AudioCD();
        
        AudioCDService ordersService = new AudioCDService(em);
        Mockito.when(ordersService.find(cd.getId())).thenReturn(null);
        
        AudioCDsController controller = new AudioCDsController(ordersService);
        
        UriComponentsBuilder uriBuilder = Mockito.mock(
            UriComponentsBuilder.class, //klasa do zamockowania
            RETURNS_DEEP_STUBS //tryb mockowania
        );
        
        // Act
        ResponseEntity result = controller.addAudioCD(cd, uriBuilder);
        
        // Assert
        assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
    }
    
    // Test 2 - crash when the cost is negative
    @Test
    public void whenAddCdWithNegativeCost_statusReturnsPaymentRequired() throws URISyntaxException
    {
        // Arrange
        AudioCD cd = new AudioCD();
        cd.setCost(-5.0);
        
        AudioCDService ordersService = new AudioCDService(em);
        Mockito.when(ordersService.find(cd.getId())).thenReturn(null);
        
        AudioCDsController controller = new AudioCDsController(ordersService);
        
        UriComponentsBuilder uriBuilder = Mockito.mock(
            UriComponentsBuilder.class, //klasa do zamockowania
            RETURNS_DEEP_STUBS //tryb mockowania
        );
        
        // Act
        ResponseEntity result = controller.addAudioCD(cd, uriBuilder);
        
        // Assert
        assertEquals(HttpStatus.PAYMENT_REQUIRED, result.getStatusCode());
    }
    
    // Test 3 - crash when the cd with given id exists (let the server do the job)
    @Test
    public void whenAddCdWithExistantId_statusReturnsConflict() throws URISyntaxException
    {
        // Arrange
        AudioCD cd1 = new AudioCD();
        AudioCD cd2 = new AudioCD();
        cd2.setId(cd1.getId());
        
        AudioCDService ordersService = new AudioCDService(em);
        Mockito.when(ordersService.find(cd2.getId())).thenReturn(cd1);
        
        AudioCDsController controller = new AudioCDsController(ordersService);
        
        UriComponentsBuilder uriBuilder = Mockito.mock(
            UriComponentsBuilder.class, //klasa do zamockowania
            RETURNS_DEEP_STUBS //tryb mockowania
        );
        
        // Act
        ResponseEntity result = controller.addAudioCD(cd2, uriBuilder);
        
        // Assert
        assertEquals(HttpStatus.CONFLICT, result.getStatusCode());
    }
    
    // Test 4 - add correct product, check if succeded
    @Test
    public void whenAddCorrectCd_checkReturnedStatusAndURI() throws URISyntaxException
    {
        // Arrange
        AudioCD cd = new AudioCD();
        cd.setAmount(10);
        cd.setAuthor("Autor");
        cd.setTitle("Tytuł");
        cd.setGenre("Genre");
        cd.setTracks(10);
        cd.setCost(14.99);
        
        AudioCDService ordersService = new AudioCDService(em);
        Mockito.when(ordersService.find(cd.getId())).thenReturn(null);
        
        AudioCDsController controller = new AudioCDsController(ordersService);
        
        UriComponentsBuilder uriBuilder = Mockito.mock(
            UriComponentsBuilder.class,
            RETURNS_DEEP_STUBS
        );
        
        URI uri = new URI("/audiocds/666");
        Mockito.when(
            uriBuilder
                .path(any())
                .buildAndExpand(cd.getId())
                .toUri()
        ).thenReturn(uri);
        
        // Act
        ResponseEntity result = controller.addAudioCD(cd, uriBuilder);
        
        // Assert
        Mockito.verify(em, times(1)).persist(cd);
        assertEquals(HttpStatus.CREATED, result.getStatusCode());
        assertEquals(uri, result.getHeaders().getLocation());
    }
    
    // Test 5 - update book, check if it got updated
    @Test
    public void whenUpdateExistingCd_checkResponse()
    {
        // Arrange
        AudioCD cd = new AudioCD();
        cd.setAmount(10);
        cd.setTracks(10);
        cd.setCost(14.99);
        
        AudioCD cd2 = new AudioCD();
        cd2.setId(cd.getId());
        cd2.setAmount(5);
        cd2.setTracks(10);
        cd2.setCost(21.99);
        
        AudioCDService ordersService = new AudioCDService(em);
        Mockito.when(ordersService.find(cd2.getId())).thenReturn(cd);
        
        AudioCDsController controller = new AudioCDsController(ordersService);
        
        // Act
        ResponseEntity result = controller.updateCd(cd2);
        
        // Assert
        Mockito.verify(em, times(1)).merge(cd2);
        assertEquals(HttpStatus.OK, result.getStatusCode());
    }
    
    // Test 6 - update not existing book, check if it got ... ???
    @Test
    public void whenUpdateNonExistingCd_checkResponse()
    {
        // Arrange
        AudioCD cd = new AudioCD();
        
        AudioCDService ordersService = new AudioCDService(em);
        Mockito.when(ordersService.find(cd.getId())).thenReturn(null);
        
        AudioCDsController controller = new AudioCDsController(ordersService);
        
        // Act
        ResponseEntity result = controller.updateCd(cd);
        
        // Assert
        assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());
    }
    
    // test7 - cena wieksza/mniesza
    @Test
    public void whenAddCdWithBiggerCost_statusReturnsPaymentRequired() throws URISyntaxException
    {
        // Arrange
        AudioCD cd = new AudioCD();
        cd.setCost(5.0);
        
        AudioCDService ordersService = new AudioCDService(em);
        Mockito.when(ordersService.find(cd.getId())).thenReturn(null);
        
        AudioCDsController controller = new AudioCDsController(ordersService);
        
        UriComponentsBuilder uriBuilder = Mockito.mock(
            UriComponentsBuilder.class, //klasa do zamockowania
            RETURNS_DEEP_STUBS //tryb mockowania
        );
        
        // Act
        ResponseEntity result = controller.addAudioCD(cd, uriBuilder);
        
        // Assert
        assertEquals(HttpStatus.FOUND, result.getStatusCode());
    }
}
